<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MonthsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('months')->insert([
            'month' => 'January',
            'quarter' => 'Q1',
        ]);
        DB::table('months')->insert([
            'month' => 'February',
            'quarter' => 'Q1',
        ]);
        DB::table('months')->insert([
            'month' => 'March',
            'quarter' => 'Q1',
        ]);
        DB::table('months')->insert([
            'month' => 'April',
            'quarter' => 'Q2',
        ]);
        DB::table('months')->insert([
            'month' => 'May',
            'quarter' => 'Q2',
        ]);
        DB::table('months')->insert([
            'month' => 'June',
            'quarter' => 'Q2',
        ]);
        DB::table('months')->insert([
            'month' => 'July',
            'quarter' => 'Q3',
        ]);
        DB::table('months')->insert([
            'month' => 'August',
            'quarter' => 'Q3',
        ]);
        DB::table('months')->insert([
            'month' => 'September',
            'quarter' => 'Q3',
        ]);
        DB::table('months')->insert([
            'month' => 'October',
            'quarter' => 'Q4',
        ]);
        DB::table('months')->insert([
            'month' => 'November',
            'quarter' => 'Q4',
        ]);
        DB::table('months')->insert([
            'month' => 'December',
            'quarter' => 'Q4',
        ]);
    }
}