<?php

use Illuminate\Database\Seeder;

class districtsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            'district' => 'Ampara',
        ]);
        DB::table('districts')->insert([
            'district' => 'Anuradhapura',
        ]);
        DB::table('districts')->insert([
            'district' => 'Badulla',
        ]);
        DB::table('districts')->insert([
            'district' => 'Colombo',
        ]);
        DB::table('districts')->insert([
            'district' => 'Galle',
        ]);
        DB::table('districts')->insert([
            'district' => 'Gampaha',
        ]);
        DB::table('districts')->insert([
            'district' => 'Hambantota'
        ]);
        DB::table('districts')->insert([
            'district' => 'Jaffna'
        ]);
        DB::table('districts')->insert([
            'district' => 'Kalutara'
        ]);
        DB::table('districts')->insert([
            'district' => 'Kandy'
        ]);
        DB::table('districts')->insert([
            'district' => 'Kegalle'
        ]);
        DB::table('districts')->insert([
            'district' => 'Kilinochchi'
        ]);
        DB::table('districts')->insert([
            'district' => 'Kurunegala'
        ]);
        DB::table('districts')->insert([
            'district' => 'Matale'
        ]);
        DB::table('districts')->insert([
            'district' => 'Matara'
        ]);
        DB::table('districts')->insert([
            'district' => 'Moneragala'
        ]);
        DB::table('districts')->insert([
            'district' => 'Mullaitivu'
        ]);
        DB::table('districts')->insert([
            'district' => 'Nuwara Eliya'
        ]);
        DB::table('districts')->insert([
            'district' => 'Polonnaruwa'
        ]);
        DB::table('districts')->insert([
            'district' => 'Puttalam'
        ]);
        DB::table('districts')->insert([
            'district' => 'Trincomalee'
        ]);
        DB::table('districts')->insert([
            'district' => 'Vavuniya'
        ]);
    }
}