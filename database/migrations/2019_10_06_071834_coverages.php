<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Coverages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coverages', function (Blueprint $table) {

            $table->increments('id');
            $table->string('year')->nullable(false);
            $table->string('month')->nullable(false);
            $table->string('quarter')->nullable(false);
            $table->string('project')->nullable(false);
            $table->string('district')->nullable(false);
            $table->string('ageBelow25')->nullable(false);
            $table->string('ageAbove25')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coverages');
    }
}
