<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use stdClass;

class Coverage extends Controller
{
    private $coverages;
    private $successStatus = 200;
    private $results = [];
    private $dataset;
    private $projects = ["MSM", "FSW", "BB", "PWID", "TGW"];
    private $estimates;
    private $target;
    private $achievement;
    private $ageAbove;
    private $summary;
    private $ageBellow;
    private $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    private $districts = ["Ampara", "Anuradhapura", "Badulla", "Batticaloa", "Colombo", "Galle", "Gampaha", "Hambantota", "Jaffna", "Kalutara", "Kandy", "Kegalle", "Kilinochchi", "Kurunegala", "Mannar", "Matale", "Matara", "Moneragala", "Mullaitivu", "Nuwara Eliya", "Polonnaruwa", "Puttalam", "Ratnapura", "Trincomalee", "Vavuniya"];
    private $quarters = ['Q1', 'Q2', 'Q3', 'Q4'];

    public function __construct()
    {
        $this->coverages = new stdClass;
        $this->dataset = [];
        $this->coverages->national = new stdClass;
        $this->coverages->national->labels = [];
        $this->coverages->national->datasets = [];

        $this->estimates = new stdClass;
        $this->estimates->label = 'Estimates';
        $this->estimates->backgroundColor = "#FEB85F";
        $this->estimates->data = [];

        $this->target = new stdClass;
        $this->target->label = 'Target';
        $this->target->backgroundColor = '#536EE3';
        $this->target->data = [];

        $this->achievement = new stdClass;
        $this->achievement->label = 'Achievement';
        $this->achievement->backgroundColor = '#02A9A1';
        $this->achievement->data = [];

        $this->ageAbove = new stdClass;
        $this->ageAbove->label = 'Age > 25';
        $this->ageAbove->backgroundColor = "#586AE6";
        $this->ageAbove->data = [];

        $this->ageBellow = new stdClass;
        $this->ageBellow->label = 'Age < 25';
        $this->ageBellow->backgroundColor = "#04A6A5";
        $this->ageBellow->data = [];
    }

    public function filter(Request $request)
    {

        if ($request->input("ageWise") == "true" && $request->input("district") == 'ALL' && $request->input("project") == 'ALL' && $request->input("quarter") == 'ALL') {

            $this->results = DB::select(
                "select SUM(`ageBelow25`) ageBelow25, SUM(`ageAbove25`) ageAbove25,
                d.district DISTRICT                
                FROM `coverages` fd
                RIGHT JOIN `districts` d ON d.district = fd.`district`
                WHERE `year` = :year
                GROUP BY fd.`district`
                ORDER BY fd.`district`",
                ["year" => $request->input("year")]
            );

            $this->summary = DB::select("select SUM(`ageBelow25`) ageBelow25, SUM(`ageAbove25`) ageAbove25 FROM `coverages` fd");

            foreach ($this->districts as $key => $district) {

                $this->ageAbove->data[$key] = 0;
                $this->ageBellow->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($district == $value->DISTRICT) {
                        $this->ageAbove->data[$key] = $value->ageAbove25;
                    }
                    if ($district == $value->DISTRICT) {
                        $this->ageBellow->data[$key] = $value->ageBelow25;
                    }
                }
            }
            $this->coverages->national->labels = $this->districts;
            $this->coverages->summary = $this->summary;
            array_push($this->coverages->national->datasets, $this->ageAbove);
            array_push($this->coverages->national->datasets, $this->ageBellow);
            return response()->json($this->coverages, $this->successStatus);
        }

        if ($request->input("district") == NULL && $request->input("project") == 'ALL' && $request->input("quarter") == 'ALL') {

            $this->results = DB::select(
                'select `type` TYPE, `project` PROJECT, SUM(`ageBelow25` + `ageAbove25`) TOTAL from `coverages` where `year` = :year group by `type`, `project`;',
                ["year" => $request->input("year")]
            );

            foreach ($this->projects as $key => $project) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($project == $value->PROJECT && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($project == $value->PROJECT && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($project == $value->PROJECT && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->projects;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages);
        } else if ($request->input("district") == NULL && $request->input("project") == 'ALL' && in_array($request->input("quarter"), $this->quarters)) {

            $this->results = DB::select(
                "select `type` TYPE, `project`,
                SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                fd.`month` MONTH
                FROM `coverages` fd
                INNER JOIN `months` m ON m.month = fd.`month`
                WHERE `project` = :id  AND `year` = :year 
                GROUP BY `type`, m.`month` ORDER BY str_to_date(m.`month`, '%M')",
                ['id' => $request->input("project"), 'year' => $request->input("year")]
            );

            foreach ($this->months as $key => $month) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($month == $value->MONTH && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if ($request->input("district") == NULL && in_array($request->input("project"), $this->projects) && $request->input("quarter") == 'ALL') {

            $this->results = DB::select(
                "select `type` TYPE, `project`,
                SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                fd.`month` MONTH
                FROM `coverages` fd
                INNER JOIN `months` m ON m.month = fd.`month`
                WHERE `project` = :id  AND `year` = :year 
                GROUP BY `type`, m.`month` ORDER BY str_to_date(m.`month`, '%M')",
                ['id' => $request->input("project"), 'year' => $request->input("year")]
            );

            foreach ($this->months as $key => $month) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($month == $value->MONTH && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if ($request->input("district") == NULL && in_array($request->input("project"), $this->projects) && in_array($request->input("quarter"), $this->quarters)) {

            $this->results = DB::select(
                "select `type` TYPE, `project`,
                SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                fd.`month` MONTH
                FROM `coverages` fd
                INNER JOIN `months` m ON m.month = fd.`month`
                WHERE `project` = :project  AND `year` = :year AND m.`quarter` = :quarter
                GROUP BY `type`, m.`month` ORDER BY str_to_date(m.`month`, '%M')",
                ['project' => $request->input("project"), 'year' => $request->input("year"), 'quarter' => $request->input("quarter")]
            );

            foreach ($this->months as $key => $month) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($month == $value->MONTH && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if ($request->input("district") == 'ALL' && $request->input("project") == 'ALL' && $request->input("quarter") == 'ALL') {

            $this->results = DB::select(
                "select `type` TYPE, SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                    d.district DISTRICT
                    FROM `coverages` fd
                    RIGHT JOIN `districts` d ON d.district = fd.`district`
                    WHERE  `year` = '2019'
                    GROUP BY `type`, fd.`district`
                    ORDER BY d.district",
                []
            );

            foreach ($this->districts as $key => $district) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($district == $value->DISTRICT && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($district == $value->DISTRICT && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($district == $value->DISTRICT && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->districts;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if ($request->input("district") == 'ALL' && in_array($request->input("project"), $this->projects) && $request->input("quarter") == 'ALL') {

            $this->results = DB::select(
                "select `type` TYPE, SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                    d.district DISTRICT
                    FROM `coverages` fd
                    RIGHT JOIN `districts` d ON d.district = fd.`district`
                    WHERE  `year` = :year AND fd.`project` = :project
                    GROUP BY `type`, fd.`district`
                    ORDER BY d.district",
                ['project' => $request->input("project"), 'year' => $request->input("year")]
            );

            foreach ($this->districts as $key => $district) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($district == $value->DISTRICT && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($district == $value->DISTRICT && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($district == $value->DISTRICT && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->districts;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if (in_array($request->input("district"), $this->districts) && $request->input("project") == 'ALL' && $request->input("quarter") == 'ALL') {

            $this->results = DB::select(
                "select `type` TYPE, SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                fd.`month` MONTH
                FROM `coverages` fd
                INNER JOIN `months` m ON m.month = fd.`month`
                WHERE `year` = :year AND fd.`district` = :district
                GROUP BY `type`, m.`month`
                ORDER BY str_to_date(m.`month`, '%M')",
                ["year" => $request->input("year"), "district" => $request->input("district")]
            );

            foreach ($this->months as $key => $month) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($month == $value->MONTH && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if (in_array($request->input("district"), $this->districts) && in_array($request->input("project"), $this->projects) && $request->input("quarter") == 'ALL') {

            $this->results = DB::select(
                "select `type` TYPE, SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                fd.`month` MONTH
                FROM `coverages` fd
                INNER JOIN `months` m ON m.month = fd.`month`
                WHERE `year` = :year AND fd.`district` = :district AND `project` = :project
                GROUP BY `type`, m.`month`
                ORDER BY str_to_date(m.`month`, '%M')",
                ['project' => $request->input("project"), "year" => $request->input("year"), "district" => $request->input("district")]
            );

            foreach ($this->months as $key => $month) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($month == $value->MONTH && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if (in_array($request->input("district"), $this->districts) && in_array($request->input("project"), $this->projects) && in_array($request->input("quarter"), $this->quarters)) {

            $this->results = DB::select(
                "select `type` TYPE, SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                fd.`month` MONTH
                FROM `coverages` fd
                INNER JOIN `months` m ON m.month = fd.`month`
                WHERE `year` = :year AND fd.`district` = :district AND `project` = :project AND `quarter` = :quarter
                GROUP BY `type`, m.`month`
                ORDER BY str_to_date(m.`month`, '%M')",
                ['project' => $request->input("project"), "year" => $request->input("year"), "district" => $request->input("district"), "quarter" => $request->input("quarter")]
            );

            foreach ($this->months as $key => $month) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($month == $value->MONTH && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else if ($request->input("district") == 'ALL' && in_array($request->input("project"), $this->projects) && in_array($request->input("quarter"), $this->quarters)) {

            $this->results = DB::select(
                "select `type` TYPE, SUM(`ageBelow25` + `ageAbove25`) TOTAL,
                fd.`month` MONTH
                FROM `coverages` fd
                INNER JOIN `months` m ON m.month = fd.`month`
                WHERE `year` = :year AND `project` = :project AND `quarter` = :quarter
                GROUP BY `type`, m.`month`
                ORDER BY str_to_date(m.`month`, '%M')",
                ['project' => $request->input("project"), "year" => $request->input("year"), "quarter" => $request->input("quarter")]
            );

            foreach ($this->months as $key => $month) {

                $this->estimates->data[$key] = 0;
                $this->target->data[$key] = 0;
                $this->achievement->data[$key] = 0;

                foreach ($this->results as $value) {

                    if ($month == $value->MONTH && $value->TYPE == 'Estimates') {
                        $this->estimates->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Target') {
                        $this->target->data[$key] = $value->TOTAL;
                    }
                    if ($month == $value->MONTH && $value->TYPE == 'Achievement') {
                        $this->achievement->data[$key] = $value->TOTAL;
                    }
                }
            }

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);

            return response()->json($this->coverages, $this->successStatus);
        } else {

            $this->coverages->national->labels = $this->months;
            array_push($this->coverages->national->datasets, $this->estimates);
            array_push($this->coverages->national->datasets, $this->target);
            array_push($this->coverages->national->datasets, $this->achievement);
            return response()->json($this->coverages, $this->successStatus);
        }
    }
}
